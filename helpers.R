#' Update texInput
#' 
#' @param input
#' @param session
#' @param prediction 
#'
updateInput <- function(input, session, prediction) {
     updateTextInput(session, "text", value = paste(input$text, prediction))
 }


#' Dummy prediction function
#' 
#' @param sentence ignored
#' @param i integer number of predictions to return
#' @return character vector
#'
get_prediction <- function(sentence,  i=1) {
    list(words = sample(c("the", "to", "and", "a", "I", "of", "in", "it", "that", "for")))
}



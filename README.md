shiny-button-example
====================

Some examples how to update `textInput` when `actionButton` is clicked.

- [buttons_observer.R](https://github.com/zero323-attic/shiny-button-example/blob/master/buttons_observer.R) - basic solution using observers,
- [border_case_reactive.R](https://github.com/zero323-attic/shiny-button-example/blob/master/border_case_reactive.R) - predictions handled inside reactive expression,

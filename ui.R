library(shiny)


ui <- shinyUI(fluidPage(
    tags$head(tags$style(
        type="text/css",
        "
        .form-group.shiny-input-container {
            width: 100%
        } 
        #text {width: 100%}
        .btn {
            display:inline-block;
            width: 100%;
        }
        "
    )),
    
    fluidRow(
        column(3),
        column(6, textInput("text", "")),
        column(3)
    ),

    fluidRow(
        column(3),
        column(6, 
            actionButton(inputId = "pred1",  label = textOutput("pred1_label"), icon = NULL)
        ),
        column(3)
    )
    
))

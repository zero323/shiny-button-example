library(shiny)
source('ui.R', local=TRUE)
source('helpers.R', local=TRUE)

runApp(shinyApp(
    ui=ui,     
    shinyServer(function(input, output,  session) {
    
        # Observers for side effects
        observe({
            if(input$pred1 == 0) return()
            isolate({
                updateInput(input, session, predictions()[1])
            })
        })

        predictions <- reactive({
            text <- input$text
            get_prediction(preprocess(text))$words
        })

        # Add render text for each button
        output$pred1_label <- renderText({
            predictions()[1]
        })
    
    })
))
